import static java.lang.Math.*;

public class Lab11 {
    public static void main(String[] args) {
        long startNumber = 0;
        int countOfOperations = 0;
        for (long inNumber = 2; inNumber < 1000000; inNumber++) {
            int count = 0;
            long outNumber = inNumber;
            while (outNumber > 1) {
                if (abs(IEEEremainder((double) outNumber, (double) 2)) == 0) {
                    outNumber = outNumber / 2;
                } else {
                    outNumber = outNumber * 3 + 1;
                }
                count++;
            }
            if (count > countOfOperations) {
                countOfOperations = count;
                startNumber = inNumber;
            }

        }
        System.out.println("start number of sequence = " + startNumber + " number of operations: " + countOfOperations);
    }
}
